#!/bin/bash

# To set environment:
# > source ./envs-example.sh

export RB_REDDIT_U=''
export RB_REDDIT_P=''
export RB_ENABLE_POSTING=1
export RB_REDIS_HOST=''
export RB_REDIS_PORT=1234
export RB_REDIS_PASS=''
