import urllib
import urllib2
import logging
import time
import os

from bs4 import BeautifulSoup
import narwal
import redis

KD_URL = 'http://karmadecay.com'

COMMENT_THRESH = int(os.getenv('RB_COMMENT_THRESH', 200))

REDDIT_U = os.getenv('RB_REDDIT_U')
REDDIT_P = os.getenv('RB_REDDIT_P')

ENABLE_POSTING = os.getenv('RB_ENABLE_POSTING', False)

REDIS_HOST = os.getenv('RB_REDIS_HOST')
REDIS_PORT = int(os.getenv('RB_REDIS_PORT'))
REDIS_PASS = os.getenv('RB_REDIS_PASS')

reddit = narwal.connect(username = REDDIT_U, password = REDDIT_P, user_agent = 'redbot')

logging.basicConfig(format = '%(asctime)s:%(levelname)s: %(message)s',
                    datefmt = '%d/%m/%Y %H:%M:%S',
                    level = logging.DEBUG)

repost_db = redis.StrictRedis(host = REDIS_HOST,
                              port = REDIS_PORT,
                              password = REDIS_PASS)

def get_soup(url):
    logging.info('Getting soup: {0}'.format(url))
    user_agent = 'Mozilla/4.0 (compatible; MSIE 5.5; Windows NT)'
    headers = { 'User-Agent' : user_agent }
    request = urllib2.Request(url, None, headers)
    response = urllib2.urlopen(request)
    html = response.read()
    logging.info('Read {0} bytes from {1}'.format(len(html), url))
    return BeautifulSoup(html)

def int_from_str(str):
    dig = (''.join(c for c in str if c.isdigit()))
    if dig.isdigit():
        return int(dig)
    else:
        return False


def post_comment(repost_path, original_thread):
    # First find the top level comment with the highest karma
    args = original_thread[22:-1].split('/')
    orig = reddit.get(*args)
    top_comment = orig[1][0].body
    time.sleep(3)

    # Post it to the repost
    args = repost_path[1:-1].split('/')
    repost = reddit.get(*args)
    time.sleep(3)
    try:
        repost[0][0].comment(top_comment)
    except narwal.exceptions.PostError as e:
        logging.error("PostError: {0}".format(e.errors))

    time.sleep(3)

def check_reposts(url):
    soup = get_soup(KD_URL + url)
    repost_results = soup.find_all('tr', class_ = 'result')

    top_comment_num = 0
    top_comment_link = None

    last_no = 0

    for repost in repost_results:
        no_contents = ""
        no_contents = str(repost.find('td', class_ = 'no').contents)
        no_int = int_from_str(no_contents)

        if no_int:
            if no_int == last_no + 1:
                last_no = no_int

                if repost.find('div', class_='commentsH'): continue

                href = repost.find('td', class_ = 'info').find('a').get('href')
                comments = repost.find('div', class_='comments')
                if comments is not None:
                    comments = str(comments.find(class_ = 'no').contents)
                    com_int = int_from_str(comments)
                    if com_int > top_comment_num:
                        top_comment_num = com_int
                        top_comment_link = href
            else:
                last_no = 0
                break

    # Did we find a repost exceeding the comment threshold?
    if top_comment_num >= COMMENT_THRESH:
        logging.info('Found viable repost with {0} comments'.format(top_comment_num))
        if ENABLE_POSTING: post_comment(url, top_comment_link)
    else:
        logging.debug('Not enough comments in any threads - max {0}'.format(top_comment_num))

def main():
    logging.info("Starting redbot cycle.")
    logging.info("Comment Threshold {0}".format(COMMENT_THRESH))
    index_soup = get_soup(KD_URL + '/reposts')

    for result in (index_soup.find_all('tr', class_ = 'result')):
        try:
            url = str(result.find('a').get('href'))
        except:
            continue

        if repost_db.get(url) is None:
            repost_db.set(url, time.time())
            repost_db.expire(url, 60*60*6) # Expire the key after 6 hours
            check_reposts(url)
        else:
            logging.debug('Found link in reposts db, skipping')

if __name__ == '__main__':
    main()
